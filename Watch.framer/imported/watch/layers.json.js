window.__imported__ = window.__imported__ || {};
window.__imported__["watch/layers.json.js"] = [
	{
		"id": 17,
		"name": "bgk",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/bgk.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "394468479"
	},
	{
		"id": 15,
		"name": "arriving",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/arriving.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 331
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "60069219"
	},
	{
		"id": 21,
		"name": "accepted invitation",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/accepted invitation.png",
			"frame": {
				"x": 9,
				"y": 24,
				"width": 303,
				"height": 249
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1804506390"
	},
	{
		"id": 30,
		"name": "directions",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/directions.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1000217669"
	}
]