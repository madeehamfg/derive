# This imports all the layers for "watch" into watchLayers
watchLayers = Framer.Importer.load "imported/watch"

notif = watchLayers["accepted invitation"]
arriving = watchLayers["arriving"]
dir = watchLayers["directions"]

green = watchLayers["bgk"]

arriving.x=-arriving.width
dir.x=-dir.width
# Welcome to Framer

notif.on Events.Click, ->
  notif.visible=false
  dir.animate
    properties: {x:0}
    curve: "ease-in"
    
    
dir.on Events.Click, ->
  dir.visible=false
  arriving.animate
    properties: {x:0}
    curve: "ease-in"