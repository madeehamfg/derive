# This imports all the layers for "Page1" into page1Layers
page1Layers = Framer.Importer.load "imported/Page1"

preferences = page1Layers["Preferences Menu"]
search=page1Layers["Search Menu"]
prefTab=page1Layers["PrefTab"]
searchTab=page1Layers["SearchTab"]
friends=page1Layers["friends"]
oldman=page1Layers["oldman"]
bear=page1Layers["bear"]
girl=page1Layers["girl"]
josh=page1Layers["josh"]
notif=page1Layers["alert"]
explore_search=page1Layers["explore_search"]
explore_friend=page1Layers["explore_friends"]
confirm=page1Layers["Confirm"]
online=page1Layers["online"]
offline=page1Layers["offline"]

preferences.x=-preferences.width
prefTab.x=preferences.width+-5
search.x=-50
prefTab.visible=true
searchTab.visible=true
friends.scale=0
notif.visible = false
confirm.scale=0

online.on Events.TouchStart, ->
  online.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
online.on Events.TouchEnd, ->
  offline.scale = online.scale
  online.animateStop()
  online.scale = 0
  offline.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
offline.on Events.TouchStart, ->
  offline.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
offline.on Events.TouchEnd, ->
  online.scale = offline.scale
  offline.animateStop()
  offline.scale = 0
  online.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"


#WORKING ON FRIENDS
#preferences.visible=false
#search.visible=false
#friends.scale=1

#WORKING ON PREFERENCES
#preferences.x=-60
#search.visible=false


# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop
# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
explore_search.on Events.TouchStart, ->
  search.animate
    properties:
      scale: .8
    curve: "spring(200,30,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
explore_search.on Events.TouchEnd, ->
  surpriseUnCheck.scale=1
  friendsUnCheck.scale=0
  friendsCheck.scale=0
  friends.scale = search.scale
  search.animateStop()
  search.scale = 0
  friends.animate
    properties:
      scale: 1
    curve: "spring(200,30,0)"
    
# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback
explore_friend.on Events.TouchStart, ->
  friends.animate
    properties:
      scale: .8
    curve: "spring(200,30,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
explore_friend.on Events.TouchEnd, ->
  checkmarkChecked1.scale=0
  checkmarkChecked2.scale=0
  checkmarkChecked3.scale=0
  checkmarkChecked4.scale=0
  surpriseCheck.scale=0
  surpriseUnCheck.scale=0
  confirm.scale = friends.scale
  friends.animateStop()
  friends.scale = 0
  confirm.animate
    properties:
      scale: 1
    curve: "spring(200,30,0)"
    
    
    
prefTab.on Events.Click, ->
	#open preferences
	if search.x > -search.width
		search.animate
			properties: {x:-search.width}
			curve: "spring(100,13,0)"
		searchTab.x=search.width-5
		friendsUnCheck.scale=0

	if preferences.x == -preferences.width
		prefTab.x=(preferences.width-prefTab.width)
		search.x=-search.width
		searchTab.visible=false
		preferences.animate
			properties: {x:-60}
			curve: "spring(100,13,0)"
		prefTab.visible=true
		
		bikeUnCheck.visible=true
		bikeUnCheck.scale=1
		wheelUnCheck.scale=1
		hillsUnCheck.scale=1
		crowdsUnCheck.scale=1
	#close preferences
	else
		preferences.animate
			properties: {x:-preferences.width}
			curve: "spring(100,13,0)"
			searchTab.x=search.width-5
		prefTab.x=(preferences.width-5)
		searchTab.visible=true

		bikeUnCheck.scale=0
		wheelUnCheck.scale=0
		hillsUnCheck.scale=0
		crowdsUnCheck.scale=0
		
searchTab.on Events.Click, ->
	#search is closed
	if search.x == -search.width
		searchTab.x=(search.width-searchTab.width)
		search.animate
			properties: {x:-50}
			curve: "spring(100,13,0)"
		friendsUnCheck.scale=1
	#search is open
	else
		search.animate
			properties: {x:-search.width}
			curve: "spring(100,13,0)"
			searchTab.x=search.width-5
		searchTab.x=(search.width-5)
		prefTab.visible=true
		friendsUnCheck.scale=0
		friendsCheck.scale=0

friendsUnCheck = new Layer x:35, y:795, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
friendsUnCheck.scale = 1
friendsCheck = new Layer x:35, y:795, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
friendsCheck.scale = 0

surpriseUnCheck = new Layer x:525, y:880, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
surpriseUnCheck.scale = 0
surpriseCheck = new Layer x:525, y:880, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
surpriseCheck.scale = 0		

friendsUnCheck.on Events.TouchStart, ->
  friendsUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
friendsUnCheck.on Events.TouchEnd, ->
  friendsCheck.scale = friendsUnCheck.scale
  friendsUnCheck.animateStop()
  friendsUnCheck.scale = 0
  friendsCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
friendsCheck.on Events.TouchStart, ->
  friendsCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
friendsCheck.on Events.TouchEnd, ->
  bikeUnCheck.scale = bikeCheck.scale
  bikeCheck.animateStop()
  bikeCheck.scale = 0
  bikeUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
surpriseUnCheck.on Events.TouchStart, ->
  surpriseUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
surpriseUnCheck.on Events.TouchEnd, ->
  surpriseCheck.scale = surpriseUnCheck.scale
  surpriseUnCheck.animateStop()
  surpriseUnCheck.scale = 0
  surpriseCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
surpriseCheck.on Events.TouchStart, ->
  surpriseCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
surpriseCheck.on Events.TouchEnd, ->
  surpriseUnCheck.scale = surpriseCheck.scale
  surpriseCheck.animateStop()
  surpriseCheck.scale = 0
  surpriseUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"

bikeUnCheck = new Layer x:35, y:770, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
bikeUnCheck.visible=false
bikeUnCheck.scale = 0
bikeCheck = new Layer x:35, y:770, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
bikeCheck.scale = 0

wheelUnCheck = new Layer x:35, y:855, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
wheelUnCheck.scale = 0
wheelCheck = new Layer x:35, y:855, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
wheelCheck.scale = 0

hillsUnCheck = new Layer x:35, y:946, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
hillsUnCheck.scale = 0
hillsCheck = new Layer x:35, y:946, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
hillsCheck.scale = 0

crowdsUnCheck = new Layer x:35, y:1033, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkUnchecked.png"
crowdsUnCheck.scale = 0
crowdsCheck = new Layer x:35, y:1033, width:50, height:50, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
crowdsCheck.scale = 0

bikeUnCheck.on Events.TouchStart, ->
  bikeUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
bikeUnCheck.on Events.TouchEnd, ->
  bikeCheck.scale = bikeUnCheck.scale
  bikeUnCheck.animateStop()
  bikeUnCheck.scale = 0
  bikeCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
bikeCheck.on Events.TouchStart, ->
  bikeCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
bikeCheck.on Events.TouchEnd, ->
  bikeUnCheck.scale = bikeCheck.scale
  bikeCheck.animateStop()
  bikeCheck.scale = 0
  bikeUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
wheelUnCheck.on Events.TouchStart, ->
  wheelUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
wheelUnCheck.on Events.TouchEnd, ->
  wheelCheck.scale = bikeUnCheck.scale
  wheelUnCheck.animateStop()
  wheelUnCheck.scale = 0
  wheelCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
wheelCheck.on Events.TouchStart, ->
  wheelCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
wheelCheck.on Events.TouchEnd, ->
  wheelUnCheck.scale = bikeCheck.scale
  wheelCheck.animateStop()
  wheelCheck.scale = 0
  wheelUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
crowdsUnCheck.on Events.TouchStart, ->
  crowdsUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
crowdsUnCheck.on Events.TouchEnd, ->
  crowdsCheck.scale = crowdsUnCheck.scale
  crowdsUnCheck.animateStop()
  crowdsUnCheck.scale = 0
  crowdsCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
crowdsCheck.on Events.TouchStart, ->
  crowdsCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
crowdsCheck.on Events.TouchEnd, ->
  crowdsUnCheck.scale = crowdsCheck.scale
  crowdsCheck.animateStop()
  crowdsCheck.scale = 0
  crowdsUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
    
# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
hillsUnCheck.on Events.TouchStart, ->
  hillsUnCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
hillsUnCheck.on Events.TouchEnd, ->
  hillsCheck.scale = hillsUnCheck.scale
  hillsUnCheck.animateStop()
  hillsUnCheck.scale = 0
  hillsCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
hillsCheck.on Events.TouchStart, ->
  hillsCheck.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
hillsCheck.on Events.TouchEnd, ->
  hillsUnCheck.scale = hillsCheck.scale
  hillsCheck.animateStop()
  hillsCheck.scale = 0
  hillsUnCheck.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# Using basic events example

# In this example, you'll learn how to use basic events to toggle a selection checkmark. Learn more about events in Framer here: http://www.framerjs.com/lessons/index.html#events

checkmarkChecked1 = new Layer x:77, y:420, width:110, height:110, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
checkmarkChecked1.scale = 0

checkmarkChecked2 = new Layer x:77, y:542, width:110, height:110, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
checkmarkChecked2.scale = 0

checkmarkChecked3 = new Layer x:80, y:661, width:110, height:110, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
checkmarkChecked3.scale = 0

checkmarkChecked4 = new Layer x:83, y:780, width:110, height:110, image:"http://examples.framerjs.com/static/examples/event-checkmark.framer/images/checkmarkChecked.png"
checkmarkChecked4.scale = 0

# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
oldman.on Events.TouchStart, ->
  oldman.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
oldman.on Events.TouchEnd, ->
  checkmarkChecked1.scale = oldman.scale
  oldman.animateStop()
  oldman.scale = 0
  checkmarkChecked1.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
checkmarkChecked1.on Events.TouchStart, ->
  checkmarkChecked1.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
checkmarkChecked1.on Events.TouchEnd, ->
  oldman.scale = checkmarkChecked1.scale
  checkmarkChecked1.animateStop()
  checkmarkChecked1.scale = 0
  oldman.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
    
    
    
# We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
bear.on Events.TouchStart, ->
	bear.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
bear.on Events.TouchEnd, ->
  checkmarkChecked2.scale = bear.scale
  bear.animateStop()
  bear.scale = 0
  checkmarkChecked2.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
checkmarkChecked2.on Events.TouchStart, ->
  checkmarkChecked2.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
checkmarkChecked2.on Events.TouchEnd, ->
  bear.scale = checkmarkChecked2.scale
  checkmarkChecked2.animateStop()
  checkmarkChecked2.scale = 0
  bear.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
    
    
    
    
 # We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
girl.on Events.TouchStart, ->
	girl.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
girl.on Events.TouchEnd, ->
  checkmarkChecked3.scale = girl.scale
  girl.animateStop()
  girl.scale = 0
  checkmarkChecked3.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
checkmarkChecked3.on Events.TouchStart, ->
  checkmarkChecked3.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
checkmarkChecked3.on Events.TouchEnd, ->
  girl.scale = checkmarkChecked3.scale
  checkmarkChecked3.animateStop()
  checkmarkChecked3.scale = 0
  girl.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
    
    
 # We want the checkmark to scale down when the user initiallyclicks or touches to provide feedback 
josh.on Events.TouchStart, ->
	josh.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
# When the click or touch ends, we want to swap the checkmark and scale back up to 1. We set checkmarkChecked to the current scale of checkmark (this makes sure they're always in sync) and then immediately set the scale of checkmark to 0. We then call animateStop() on checkmark, in case the spring is still oscillating, then set checkmark's scale to 0 and finally animate checkmarkChecked back to the proper scale.
josh.on Events.TouchEnd, ->
  checkmarkChecked4.scale = josh.scale
  josh.animateStop()
  josh.scale = 0
  checkmarkChecked4.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
# We also want to be able to toggle the selection state of the checkmark, so we apply the same events with the opposite effects to checkmarkChecked
checkmarkChecked4.on Events.TouchStart, ->
  checkmarkChecked4.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
checkmarkChecked4.on Events.TouchEnd, ->
  josh.scale = checkmarkChecked4.scale
  checkmarkChecked4.animateStop()
  checkmarkChecked4.scale = 0
  josh.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"

