# This imports all the layers for "Page1" into page1Layers
page1Layers = Framer.Importer.load "imported/Page1"

preferences = page1Layers["Preferences Menu"]
search=page1Layers["Search Menu"]
friends=page1Layers["friends"]
notif=page1Layers["alert"]
confirm=page1Layers["Confirm"]
accept=page1Layers["accept_notif"]
online=page1Layers["online"]
offline=page1Layers["offline"]

preferences.visible=false
search.visible=false
friends.visible=false
confirm.scale=0
# Welcome to Framer


accept.on Events.TouchStart, ->
  notif.animate
    properties:
      scale: .8
    curve: "spring(200,30,0)"
    
accept.on Events.TouchEnd, ->
  confirm.scale = notif.scale
  notif.animateStop()
  notif.scale = 0
  confirm.animate
    properties:
      scale: 1
    curve: "spring(200,30,0)"
    
online.on Events.TouchStart, ->
  online.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
online.on Events.TouchEnd, ->
  offline.scale = online.scale
  online.animateStop()
  online.scale = 0
  offline.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
    
offline.on Events.TouchStart, ->
  offline.animate
    properties:
      scale: .8
    curve: "spring(200,15,0)"
    
offline.on Events.TouchEnd, ->
  online.scale = offline.scale
  offline.animateStop()
  offline.scale = 0
  online.animate
    properties:
      scale: 1
    curve: "spring(200,15,0)"
